import 'dotenv/config';

export default ({ config }) => ({
  ...config,
  extra: {
    firebase: process.env.FIREBASE,
  },
});
